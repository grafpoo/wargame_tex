# A style file to make wargames in LaTeX

This allows one to create a document that contains all that is needed
for a wargame: 

- A board of hexes 
- Chits (_counters_ is already taken by TeX) 
- Examples 
- Rules 

The package uses [Ti_k_Z](https://github.com/pgf-tikz/pgf) for most things. 

The sub directory `world` contains data and
[http://www.gnuplot.info/](GNUPlot) code to make Ti_k_Z paths for coast
lines and similar.

## Download 

- [Zip file of package and support
  files](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/download?job=dist)
- [Browse content of
  package](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/browse?job=dist)
- [Documentation](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/texmf/doc/latex/wargame/wargame.pdf?job=dist)
- [Table of symbols](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/texmf/doc/latex/wargame/symbols.pdf?job=dist)
- [Compatibility](https://gitlab.com/wargames_tex/wargame_tex/-/jobs/artifacts/master/file/public/texmf/doc/latex/wargame/compat.pdf?job=dist)

## Examples 

Below are some print'n'play board wargames made with this package.
These are not original games but rather revamps of existing games.
All credits goes to the original authors of these games. 

- WWII 
  - Eastern Front 
    - [Battle for Moscow](https://gitlab.com/wargames_tex/bfm_tex) 
    - [Hell's Gate](https://gitlab.com/wargames_tex/hellsgate_tex)
    - [Unternehmung 25](https://gitlab.com/wargames_tex/unternehmung_tex) (private)
  - Western Front
    - [Paul Koenig's Market Garden](https://gitlab.com/wargames_tex/pkmg_tex)
    - [Paul Koenig's D-Day](https://gitlab.com/wargames_tex/pkdday_tex)
  - Pacific theatre 
    - [First Blood](https://gitlab.com/wargames_tex/firstblood_tex)
    - [Malaya](https://gitlab.com/wargames_tex/malaya_tex) (private)
  - Africa 
    - [Battles of El Alamein](https://gitlab.com/wargames_tex/boel_tex) (TBD)
- Modern/Speculative
  - [Strike Force One](https://gitlab.com/wargames_tex/sfo_tex) 
  - [Kriegspiel](https://gitlab.com/wargames_tex/kriegspiel_tex)


## References 

- Wikipedia page on NATO symbols 

	https://en.wikipedia.org/wiki/NATO_Joint_Military_Symbology

- NATO page on standard 

    https://nso.nato.int/nso/nsdd/main/standards/ap-details/1912/EN
	
	
  If the above link does not work, go to 
  
    https://nso.nato.int/nso/nsdd/main/standards 
	
  and write in `SYMBOLOGY` in the _Document Title Contains Words_
  field and press _Start_.
  
- XMilSymb - other LaTeX package for NATO symbology

    https://github.com/ralphieraccoon/MilSymb
	

	
