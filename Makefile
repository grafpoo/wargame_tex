#
#
#
NAME		:= wargame
VERSION		:= 0.1
LATEX_FLAGS	:= -interaction=nonstopmode 	\
		   -file-line-error		\
		   --synctex=15			\
		   -shell-escape
LATEX		:= pdflatex  
MAKEINDEX	:= makeindex
SOURCES		:= wargame.ins				\
		   wargame.dtx				\
		   package.dtx				\
		   util/core.dtx			\
		   chit/shape.dtx			\
		   chit/misc.dtx			\
		   chit/elements.dtx			\
		   chit/core.dtx			\
		   hex/shape.dtx			\
		   hex/tile.dtx				\
		   hex/ridges.dtx			\
		   hex/towns.dtx			\
		   hex/terrain.dtx			\
		   hex/paths.dtx			\
		   hex/core.dtx				\
		   hex/labels.dtx			\
		   hex/extra.dtx			\
		   hex/terrain/woods.dtx		\
		   hex/terrain/town.dtx			\
		   hex/terrain/tree.dtx			\
		   hex/terrain/beach.dtx		\
		   hex/terrain/mountain.dtx		\
		   hex/terrain/light_woods.dtx		\
		   hex/terrain/mountains.dtx		\
		   hex/terrain/village.dtx		\
		   hex/terrain/city.dtx			\
		   hex/terrain/swamp.dtx		\
		   hex/terrain/rough.dtx		\
		   hex/board.dtx			\
		   hex/coord.dtx			\
		   natoapp6c/shape.dtx			\
		   natoapp6c/symbols.dtx		\
		   natoapp6c/list.dtx			\
		   natoapp6c/compat/seasurface.dtx	\
		   natoapp6c/compat/activity.dtx	\
		   natoapp6c/compat/subsurface.dtx	\
		   natoapp6c/compat/missile.dtx		\
		   natoapp6c/compat/air.dtx		\
		   natoapp6c/compat/seamine.dtx		\
		   natoapp6c/compat/land.dtx		\
		   natoapp6c/compat/equipment.dtx	\
		   natoapp6c/compat/installation.dtx	\
		   natoapp6c/compat/space.dtx		\
		   natoapp6c/frames/hostile.dtx		\
		   natoapp6c/frames/base.dtx		\
		   natoapp6c/frames/friendly.dtx	\
		   natoapp6c/frames/neutral.dtx		\
		   natoapp6c/frames/unknown.dtx		\
		   natoapp6c/weaponry.dtx		\
		   natoapp6c/core.dtx			\
		   natoapp6c/text.dtx			\
		   natoapp6c/echelon.dtx		\
		   natoapp6c/util.dtx			\
		   tests/map.dtx			\
		   tests/chits.dtx			

DESTDIR		:= $(HOME)/
instdir		:= $(DESTDIR)/texmf/tex/latex/wargame
TILES		:= beach	\
		   city		\
		   light_woods	\
		   mountains	\
		   rough	\
		   swamp	\
		   town		\
		   village	\
		   woods	
TABLES		:= air		\
		   missile	\
		   land		\
		   equipment	\
		   installation	\
		   seasurface	\
		   subsurface	\
		   seamine	\
		   space	\
		   activity
PKG_FILES	:= wargame.sty					\
		   tikzlibrarywargame.util.code.tex		\
		   tikzlibrarywargame.hex.code.tex		\
		   tikzlibrarywargame.natoapp6c.code.tex	\
		   tikzlibrarywargame.chit.code.tex		\
		   $(TILES:%=%.pdf)				

# 		   beach.png					\
# 		   city.png					\
# 		   mountains.png					\
# 		   sea.png    					\
# 		   woods.png					\
# 		   light_woods.png  				\
# 		   rough.png     				\
# 		   swamp.png					\
# 		   town.png
DOC_FILES	:= wargame.pdf symbols.pdf compat.pdf

ifdef VERBOSE	
MUTE		:=
REDIR		:=
LATEX_FLAGS	:= 
else
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
endif
ifdef CI_COMMIT_REF_NAME
VERSION		:= $(CI_COMMIT_REF_NAME)
else
ifdef 		:= $(CI_JOB_ID)
endif

%.aux:%.tex
	@echo "First LaTeX pass of TEX $< to make $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $<  $(REDIR)

%.idx:%.dtx
	@echo "First LaTeX pass of DTX $< to make $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $<  $(REDIR)

%.aux:%.idx
	@echo "Second LaTeX pass of DTX $*.dtx (via $<) make to $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $*.dtx  $(REDIR)
	$(MUTE)touch $< $@

%.pdf:%.aux %.ind
	@echo "Third LaTeX pass of DTX $*.dtx (via $<) to make $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $*.dtx  $(REDIR)
	$(MUTE)touch $^ $@

%.pdf:%.aux %.tex
	@echo "Second LaTeX pass of TEX $*.tex (via $<) to make $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $*.tex  $(REDIR)

%.pdf:%.tex
	@echo "Single LaTeX pass of TEX $< to make $@"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $<  $(REDIR)


%.ind:%.idx
	@echo "Make index $@ from $<"
	$(MUTE)$(MAKEINDEX) -s gind -o $@ $<  $(REDIR)
	$(MUTE)touch $^ $@

%.gls:%.glo
	@echo "Make glossary $@ from $<"
	$(MUTE)$(MAKEINDEX) -s gglo -o $@ $<  $(REDIR)
	$(MUTE)touch $^ $@

all:		wargame.pdf

package:	$(PKG_FILES)

everything:	all symbols.pdf compat.pdf tests/test.pdf

clean:
	@echo "Cleaning"
	$(MUTE)rm -f *~ *.log* *.aux *.toc *.lof *.lot *.out *.ilg *.idx *.ind
	$(MUTE)rm -f cmp_*.tex *.synctex* *.hd
	$(MUTE)rm -f symbols.tex compat.tex testmap.tex testchits.tex 
	$(MUTE)rm -f wargame.sty wgdoc.sty logo.png logo.pdf
	$(MUTE)rm -f symbols.pdf compat.pdf test.pdf wargame.pdf
	$(MUTE)rm -f tikzlibrarywargame.util.code.tex
	$(MUTE)rm -f tikzlibrarywargame.hex.code.tex
	$(MUTE)rm -f tikzlibrarywargame.natoapp6c.code.tex
	$(MUTE)rm -f tikzlibrarywargame.chit.code.tex
	$(MUTE)rm -f $(TILES:%=%.tex) $(TILES:%=%.pdf) 
	$(MUTE)rm -rf $(NAME)-$(VERSION) $(NAME)-$(VERSION).zip

install-devel: all 
	(cd $(dir $(instdir)) && \
		rm -f $(notdir $(instdir)) && \
		ln -fs $(PWD) $(notdir $(instdir)))

install: instdir
	cp -a $(SOURCES) $(instdir)/

uninstall:
	rm -rf $(instdir)

instdir:
	mkdir -p $(instdir)



wargame.sty 				\
testmap.tex 				\
testchits.tex 				\
wgdoc.sty 				\
compat.tex 				\
symbols.tex 				\
tikzlibrarywargame.hex.code.tex		\
tikzlibrarywargame.natoapp6c.code.tex	\
tikzlibrarywargame.chit.code.tex	\
$(TILES:%=%.tex)			\
$(TABLES:%=cmp_%.tex): $(SOURCES)
	@echo "Generate package, Tikz libraries, etc from $<"
	$(MUTE)$(LATEX) $< $(REDIR)

wargame.idx:	wargame.dtx wargame.sty $(TILES:%=%.pdf)
wargame.pdf:	wargame.aux wargame.ind
fast:		wargame.idx
symbols.aux:	symbols.tex wargame.sty
compat.aux:	compat.tex $(TABLES:%=cmp_%.tex) wargame.sty
test.aux:	test.tex wargame.sty

beach.pdf:	beach.tex
city.pdf:	city.tex
light_woods.pdf:light_woods.tex
mountains.pdf:	mountains.tex
rough.pdf:	rough.tex
swamp.pdf:	swamp.tex
town.pdf:	town.tex
village.pdf:	village.tex
woods.pdf:	woods.tex

distdir:$(PKG_FILES) $(DOC_FILES)
	mkdir -p $(DESTDIR)$(NAME)-$(VERSION)/texmf/tex/latex/wargame
	mkdir -p $(DESTDIR)$(NAME)-$(VERSION)/texmf/doc/latex/wargame
	cp $(PKG_FILES) $(DESTDIR)$(NAME)-$(VERSION)/texmf/tex/latex/wargame/
	cp $(DOC_FILES) $(DESTDIR)$(NAME)-$(VERSION)/texmf/doc/latex/wargame/

dist:	distdir
	zip $(NAME)-$(VERSION).zip -r $(NAME)-$(VERSION)/
	rm -rf $(NAME)-$(VERSION)

distdir:		DESTDIR=$(PWD)/
dist:			DESTDIR=$(PWD)/

logo.png:logo.pdf
	pdftocairo -png $<
	mv logo-1.png logo.png

docker:
	docker run --user root --group-add users -e GRANT_SUDO=yes -it --rm \
		-v $(PWD):/root/$(notdir $(PWD)) texlive/texlive \
		/bin/bash

docker-artifacts: everything
ifneq ($(DESTDIR),$(HOME)/)
	mkdir -p $(DESTDIR)texmf/tex/latex/wargame
	mkdir -p $(DESTDIR)texmf/doc/latex/wargame
	cp $(PKG_FILES) $(DESTDIR)texmf/tex/latex/wargame/
	cp $(DOC_FILES) $(DESTDIR)texmf/doc/latex/wargame/
endif


#
# EOF
#
