#!/usr/bin/env python3

import markdown as md
import html.parser
import sys

class Parser(html.parser.HTMLParser):
    NONE  = 0
    START = 1
    SUB   = 2
    LIST  = 3
    ITEM  = 4
    
    def __init__(self,base=False):
        super(Parser,self).__init__()
        self._data  = list()
        self._state = Parser.NONE
        self._facs  = ['friendly','hostile','neutral','unknown']
        self._out   = None
        if base:
            self._facs = ['none,frame draw=lightgray']+self._facs
    def __del__(self):
        self.end_file()
        
    def p(self,*args,**kwargs):
        print(*args,**kwargs,file=self._out)
            
    def begin_file(self):
        self.end_file()

        fn = 'tbl_'+self._cmd.replace(' ','')
        print(r'\include{'+fn+'}',file=sys.stderr)
        self._out = open(fn+'.tex','w')
                         
        hd = r'\def\colheaders{%' + '\n'\
             r'  \hline%' + '\n'\
             r'  &&\multicolumn{'+str(len(self._facs))+'}{c|}'\
             r'{\targ{faction}}\\%' + '\n'\
             r'  \textsf{milsymb}' + '\n'\
             r'  & Keys' + '\n'
        for f in self._facs:
            hd += r'  & \tspec{'+f+'}%\n'
        hd += r'  \\%' + '\n'\
              r'  \hline}'
        self.p(hd)
        self.p(r'\def\tspec#1{{\ttfamily\footnotesize\obeyspaces #1}}')
        self.p(r'\def\targ#1{$\langle${\ttfamily\small\itshape #1}$\rangle$}')
        self.p(r'\def\continued{%'+'\n'\
              r'  \multicolumn{'+str(2+len(self._facs))+'}{c}'\
              r'{\textit{\small continued from previous page}}\\}')
        self.p(r'\def\continues{%'+'\n'\
              r'  \multicolumn{'+str(2+len(self._facs))+'}{c}'\
              r'{\textit{\small continues on next page}}\\}')

    def end_file(self):
        if self._out is None:
            return

        self._out.close()
        self._out = None
    
    def begin_tbl(self):
        self.p(r'\begin{longtable}{|m{.13\linewidth}m{.25\linewidth}|',end='')
        for c in range(len(self._facs)):
            self.p(r'm{.06\linewidth}',end='')
        self.p('|}\n'
              r'  \caption{\texttt{command='+self._cmd+r'}}\\' + '\n'
              r'  \colheaders' + '\n'
              r'  \endfirsthead' + '\n'
              r'  \continued' + '\n'
              r'  \colheaders' + '\n'
              r'  \endhead' + '\n'
              r'  \continues' + '\n'
              r'  \endfoot' + '\n'
              r'  \endlastfoot')

    def end_tbl(self):
        self.p(r'\end{longtable}')
        
    def handle_starttag(self,tag,attrs):
        if tag == 'h1':
            self._state = Parser.START
        elif tag == 'h2':
            self._state = Parser.SUB
        elif tag == 'ul':
            self._state = Parser.LIST
            self.begin_tbl()
        elif tag == 'li':
            self._state = Parser.ITEM

        # print("Got " + tag,file=sys.stderr)

    def handle_endtag(self,tag):
        if tag == 'h1':
            self._state = Parser.NONE
        elif tag == 'h2':
            self._state = Parser.START
        elif tag == 'ul':
            self.end_tbl()
            self._state = Parser.SUB
        elif tag == 'li':
            self._state = Parser.LIST
        else:
            print("End of " + tag,file=sys.stderr)

    def handle_data(self,data):
        if self._state == Parser.START:
            self.section(1,data)
        elif self._state == Parser.SUB:
            self.section(2,data)
        elif self._state == Parser.LIST:
            pass
        elif self._state == Parser.ITEM:
            self.line(data)

        # print('Got data "{}"'.format(data),file=sys.stderr)

        
    def section(self,lvl,data):
        if len(data) <= 0:
            return

        txt = data.split()
        if lvl == 1:
            if len(txt) <= 1:
                # print('Got too little data at level 1: '+data,file=sys.stderr)
                return

            self._cmd  = ' '.join(txt[:-1]).lower()
            txt        = r'\spec{'+self._cmd+'} Command'
            cmd        = r'\subsection'
            self.begin_file()
        else:
            if len(txt) <= 0:
                # print('Got too little data at level 2: '+data,file=sys.stderr)
                return
            
            self._what = txt[0].lower()
            txt        = r'\spec{' + self._what + '}'
            cmd        = r'\subsubsection'

        self.p(cmd + '{' + txt + '}\n')

    def parse(self,spec,idef,nms,flds):
        for s,f in zip(nms,flds):
            if spec.startswith(s+'='):
                f += [spec.replace(s+'=','')]
                return flds

        flds[idef] += [spec]
        return flds
            
    def line(self,data):
        if len(self._cmd) <= 0:
            print('No command given!',file=sys.stderr)
            return
        if len(self._what) <= 0:
            print('No what given!',file=sys.stderr)
            return
        
        lines = data.split('\n')
        if len(lines) < 2:
            raise ValueError(f'No line to split: {data} -> {lines}')
        mil   = lines[0].strip()
        spec  = lines[1].strip()
        what  = self._what

        self.p(r'  \raggedright\tspec{' + mil + '}')

        if 'TBD'     in spec:
            self.p(r'  &\raggedright\tspec{' + spec + '}')
            self.p(r'  &\multicolumn{'+str(len(self._facs))
                  +'}{c|}{To be done}\\\\\n  \\hline')
            return
        
        main=[]
        left=[]
        right=[]
        top=[]
        bottom=[]
        below=[]
        nms=['main','left','right','upper','lower','below']
        fls=[main,left,right,top,bottom,below]
        for s in spec.split(','):
            self.parse(s,nms.index(what),nms,fls)
            
        sp = []
        for n,s in zip(nms,fls):
            if len(s) <= 0 or len(s[0]) <= 0:
                continue
            sp += [n+'={'+','.join(s)+'}']
        sp = ','.join(sp)
        # print(sp,file=sys.stderr)

        tsp = sp.replace('\\','\\textbackslash ')\
                .replace('{',r'\{')\
                .replace('}',r'\}')\
                .replace(',',', ')                        
        self.p(r'  &\raggedright\tspec{' + tsp + '}')
        for f in self._facs:
            cmd='  &\\tikz[scale=.6]{\n'\
                '     \\natoapp[faction='+f+',\n'\
                '              command='+self._cmd+',\n'\
                '              frame fill=faction,\n'\
                '              '+sp+']}'

            self.p(cmd)
        self.p('  \\\\\n  \\hline')
        
    def run(self):
        with open('status.md') as file:
            super(Parser,self).feed(md.markdown(file.read()))


if __name__ == '__main__':
    p = Parser(True)
    p.run()




        

        
